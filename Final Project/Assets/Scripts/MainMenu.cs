﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class MainMenu : MonoBehaviour
{
    public string playGameLevel;
    public GameObject controlsPanel;
    public Text highScoreText;
    private float hiScore;

    void Start()
    {
        hiScore = PlayerPrefs.GetFloat("hiScore");
        highScoreText.text = "High Score: " + Mathf.Round(hiScore);
    }

    public void Play()
    {
        SceneManager.LoadScene(playGameLevel);
    }

    public void ShowControls()
    {
        controlsPanel.SetActive(true);
    }

    public void HideControls()
    {
        controlsPanel.SetActive(false);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
