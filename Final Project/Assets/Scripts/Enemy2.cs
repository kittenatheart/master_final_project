﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2 : MonoBehaviour
{
    Rigidbody2D rB2D;
    SpriteRenderer sprite;
    AudioSource death;

    public float moveSpeed;
    private float moveSpeedStore;

    public float timeOne;
    public float waitTime;
    public float lifeTime;
    private float timer;
    private float timerMove;
    private float lifeTimer;

    public GameObject player;
    private float playerSpeed;

    void Start()
    {
        rB2D = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
        death = GetComponent<AudioSource>();
        turnOnOff();

        moveSpeedStore = moveSpeed;
        timer = timeOne;
        timerMove = 6f;
        lifeTimer = lifeTime;

        playerSpeed = player.GetComponent<PlayerController>().runSpeed;
    }

    void Update()
    {
        //Check player speed
        playerSpeed = player.GetComponent<PlayerController>().runSpeed;

        //Movement
        rB2D.velocity = new Vector2(moveSpeed, rB2D.velocity.y);

        //Triggering speed shift
        timerMove -= Time.deltaTime;
        if (timerMove < 0)
        {
            if (player.transform.position.x - transform.position.x < 1.5f)
            {
                moveSpeed = playerSpeed;
                timerMove = Random.Range(5, 7);
            }
            else
            {
                moveSpeed = Random.Range(playerSpeed - 2, playerSpeed + 2);
                if (moveSpeed > 16)
                {
                    moveSpeed = playerSpeed;
                    timerMove = Random.Range(1, 2);
                }
                else if(moveSpeed > playerSpeed)
                {
                    timerMove = Random.Range(1, 2);
                }
                else if (moveSpeed == playerSpeed)
                {
                    timerMove = Random.Range(3, 5);
                }
                else
                {
                    timerMove = Random.Range(1, 2);
                }
            }
        }

        if (timer > 0)
        {
            timer -= Time.deltaTime;
        }
        //Triggering event
        timer -= Time.deltaTime;
        if (timer < 0)
        {
            Restart();
        }

        lifeTimer -= Time.deltaTime;
        if (player.transform.position.x - transform.position.x > 25 || lifeTimer<0)
        {
            Die();
        }
    }

    private void turnOnOff()
    {
        GetComponent<Collider2D>().enabled = !GetComponent<Collider2D>().enabled;
        sprite.enabled = !sprite.enabled;
    }

    //restarts enemy attack
    private void Restart()
    {
        transform.position = new Vector3(player.transform.position.x - 20f, -1.31f, transform.position.z);
        turnOnOff();
        timerMove = 6f;
        moveSpeed = moveSpeedStore;
        timer = 20f;
        lifeTimer = lifeTime;
    }
    //deactivates enemy in the scene and resets it for next activation
    public void Die()
    {
        death.Play();
        turnOnOff();
        timer = timeOne + waitTime;
        transform.position = new Vector3(player.transform.position.x - 20f, -1.31f, transform.position.z);
    }
}
