﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public Transform platformGenerator;
    private Vector3 platformStart;

    public PlayerController player;
    private Vector3 playerStart;

    private PlatformDestroyer[] platformList;


    void Start()
    {
        platformStart = platformGenerator.position;
        playerStart = player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RestartGame()
    {
        StartCoroutine("RestartGameCo");
    }

    public IEnumerator RestartGameCo()
    {
        player.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        platformList = FindObjectsOfType<PlatformDestroyer>();
        for(int i=0; i<platformList.Length; i++)
        {
            platformList[i].gameObject.SetActive(false);
        }

        player.transform.position = playerStart;
        platformGenerator.position = platformStart;
        player.gameObject.SetActive(true);
    }
}
