﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public Text scoreText;
    public Text hiScoreText;

    public float score;
    public float hiScore;

    public float pointsPerSecond;

    public Rigidbody2D player;

    void Start()
    {
       
    }

    void Update()
    {
        if(player.velocity.x>0)
        {
            score += pointsPerSecond * Time.deltaTime;

            if (score > hiScore)
            {
                hiScore = score;
            }

            scoreText.text = "Score " + Mathf.Round(score);
            hiScoreText.text = "High Score " + Mathf.Round(hiScore);
        }
    }

    void OnEnable()
    {
        hiScore = PlayerPrefs.GetFloat("hiScore");
    }

    void OnDisable()
    {
        PlayerPrefs.SetFloat("hiScore", hiScore);
    }
}
