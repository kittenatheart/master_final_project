﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Security.Cryptography;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
	Rigidbody2D rB2D;

	public float runSpeed;
	public float[] runSpeeds;
	private AudioSource song;
	public AudioSource[] songs;
	private int cur;
	private bool paused;

	public float jumpForce;
	public float jumpTime;
	private float jumpTimer;

	public Animator animator;

	public bool grounded;
	public LayerMask level;
	public LayerMask enemy1;
	public GameObject groundCheck;

	public GameObject deathScreen;

	public GameManager gameManager;



	void Start()
	{
		rB2D = GetComponent<Rigidbody2D>();
		//animator = gameObject.transform.GetChild(0).GetComponent<Animator>();
		//animator.SetBool("isRunning", true);
		song = GetComponent<AudioSource>();

		cur = 0;
		jumpTimer = jumpTime;
	}

	void Update()
	{
		grounded = Physics2D.IsTouchingLayers(groundCheck.GetComponent<Collider2D>(), level) || Physics2D.IsTouchingLayers(groundCheck.GetComponent<Collider2D>(), enemy1);

		//Movement
		rB2D.velocity = new Vector2(runSpeed, rB2D.velocity.y);

		//int levelMask = LayerMask.GetMask("Level");
		//Jump
		if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.W))
		{
			if (grounded/*Physics2D.BoxCast(transform.position, new Vector2(1f, .1f), 0f, Vector2.down, 0.1f, levelMask)*/)
			{
				rB2D.velocity = new Vector2(rB2D.velocity.x, jumpForce);
			}
		}
		//Hold jump
		if (Input.GetKey(KeyCode.Space) || Input.GetMouseButton(0) || Input.GetKey(KeyCode.W))
		{
				if (jumpTimer > 0)
				{
					rB2D.velocity = new Vector2(rB2D.velocity.x, jumpForce);
					jumpTimer -= Time.deltaTime;
				}
		}
		if (Input.GetKeyUp(KeyCode.Space) || Input.GetMouseButtonUp(0) || Input.GetKeyUp(KeyCode.W))
		{
			jumpTimer = 0;
		}
		if (grounded)
		{
			jumpTimer = jumpTime;
		}


		//song change/speed change (go forward a song)
		if(Input.GetKeyDown(KeyCode.D))
        {
			if(cur<runSpeeds.Length-1)
            {
				cur++;
            }
            else
            {
				cur = 0;
            }
			runSpeed = runSpeeds[cur];
			song.mute = true;
			song = songs[cur];
			song.mute = false;
        }

		//song change/speed change (go back a song)
		if (Input.GetKeyDown(KeyCode.A))
		{
			if (!paused)
			{
				if (cur > 0)
				{
					cur--;
				}
				else
				{
					cur = runSpeeds.Length - 1;
				}
				runSpeed = runSpeeds[cur];
				song.mute = true;
				song = songs[cur];
				song.mute = false;
			}
		}
		
		 //pause music
		if(Input.GetKeyDown(KeyCode.S))
        {
			paused = true;
			if(runSpeed == 0)
			{
				runSpeed = runSpeeds[cur];
				song.Play();
				animator.SetBool("isRunning", true);
			}
			else
			{
				runSpeed = 0;
				song.Pause();
				animator.SetBool("isRunning", false);
			}
		}
	}

	void OnTriggerEnter2D(Collider2D other)
    {
		if(other.CompareTag("killBox"))
        {
			deathScreen.SetActive(true);
			gameObject.SetActive(false);
			//gameManager.RestartGame();
        }
    }
	
}
