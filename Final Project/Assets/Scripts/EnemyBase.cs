﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBase : MonoBehaviour
{
    Rigidbody2D rB2D;
    SpriteRenderer sprite;

    public float moveSpeed;
    private float moveSpeedStore;

    public float timeOne;
    public float waitTime;
    private float timer;
    private float timerMove;

    public GameObject player;

    void Start()
    {
        rB2D = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
        turnOnOff();

        moveSpeedStore = moveSpeed;
        timer = timeOne;
        timerMove = Random.Range(5, 9);
    }

    void Update()
    {
        //Movement
        rB2D.velocity = new Vector2(moveSpeed, rB2D.velocity.y);

        //Triggering speed shift
        timerMove -= Time.deltaTime;
        if (timerMove < 0)
        {
            moveSpeed = Random.Range(5, 10);
            if (moveSpeed > 7)
            {
                timerMove = Random.Range(2, 5);
            }
            else if (moveSpeed > 6)
            {
                timerMove = Random.Range(3, 5);
            }
            else
            {
                timerMove = Random.Range(1, 2);
            }
        }

        //Triggering event
        timer -= Time.deltaTime;
        if (timer < 0)
        {
            transform.position = new Vector3(player.transform.position.x - 6, transform.position.y, transform.position.z);
            turnOnOff();
            timer = 2000f;
        }
    }

    private void turnOnOff()
    {
        GetComponent<Collider2D>().enabled = !GetComponent<Collider2D>().enabled;
        sprite.enabled = !sprite.enabled;
    }

    //deactivates enemy in the scene and resets it for next activation
    public void Die()
    {
        turnOnOff();
        timer = timeOne + waitTime;
        timerMove = Random.Range(5, 9);
        moveSpeed = moveSpeedStore;
    }
}
