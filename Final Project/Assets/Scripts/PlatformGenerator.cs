﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformGenerator : MonoBehaviour
{
    public Transform generationPoint;
    public float distanceBetween; //distance between platforms; changes but is constrained
    public float distanceBetweenMin;
    public float distanceBetweenMax;


    //creates a collection of platforms to choose from
    public ObjectPooler[] objectPools;
    //creates a collection of widths that will correspond to the platforms beign used in the object pools
    private float platformWidth;
    private float[] platformWidths;
    private int platformSelector;

    
    //like distanceBetween, but for y instead of x
    private float minHeight;
    public Transform maxHeightPoint;
    private float maxHeight;
    public float maxHeightChange;
    private float heightChange;

    void Start()
    {
        //gets the length of each platform being used
        platformWidths = new float[objectPools.Length];
        for(int i=0; i<objectPools.Length; i++)
        {
            platformWidths[i] = objectPools[i].pooledObject.GetComponent<BoxCollider2D>().size.x;
        }
        

        //sets the minHeight to the height of the generation point at the first platform
        minHeight = transform.position.y;
        //sets the maxHeight to the height of the game object in the scene
        maxHeight = maxHeightPoint.position.y;
        
    }

    void Update()
    {
        //if the generator object is behind the generation point, it will create another platform
        if(transform.position.x < generationPoint.position.x)
        {
            //picks a random distance within the range of the player's jump
            distanceBetween = Random.Range(distanceBetweenMin, distanceBetweenMax);
            //picks a random platform from the collection of platforms available
            platformSelector = Random.Range(0, objectPools.Length);
            //picks a random height up or down within the player's jump from their current position
            heightChange = transform.position.y + Random.Range(maxHeightChange, -maxHeightChange);
            if(heightChange > maxHeight)
            {
                heightChange = maxHeight;
            }
            else if(heightChange < minHeight)
            {
                heightChange = minHeight;
            }

            //moves the generator forward to the next empty spot
            transform.position = new Vector3(transform.position.x + (platformWidths[platformSelector]/2) + distanceBetween, heightChange, transform.position.z);

            //creates a new platform at the empty spot where the generator just left
                //selects a new platform from the pool of platforms we already have (or makes one to fill the position) and turns it on
            GameObject newPlatform = objectPools[platformSelector].GetPooledObjects();
            newPlatform.transform.position = transform.position;
            newPlatform.transform.rotation = transform.rotation;
            newPlatform.SetActive(true);
            
            //moves the generator to the end of the newly made platform
            transform.position = new Vector3(transform.position.x + (platformWidths[platformSelector] / 2), transform.position.y, transform.position.z);

        }
    }
}
