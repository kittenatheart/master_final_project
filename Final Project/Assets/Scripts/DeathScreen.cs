﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathScreen : MonoBehaviour
{
    //death screen music clip
    public AudioClip music;

    IEnumerator Start()
    {
        AudioSource audio = GetComponent<AudioSource>();

        //death screen sound effect starting
        audio.Play();
        //timer while this clip plays
        yield return new WaitForSeconds(audio.clip.length);
        //switches to the music clip
        audio.clip = music;
        //plays the music
        audio.Play();
    }

    public void Restart()
    {
        SceneManager.LoadScene(1);
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(0);
    }
}
